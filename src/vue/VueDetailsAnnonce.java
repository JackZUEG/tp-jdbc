package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.SimpleDateFormat;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import controleur.FaireOffreControleur;
import modele.*;
public class VueDetailsAnnonce extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public VueDetailsAnnonce(Annonce annonce) {
		
		setContentPane(createContent(annonce));
		
		setTitle("Details de l'annonce");
		
		setPreferredSize(new Dimension(600, 480));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
	}

	private JPanel createContent(Annonce annonce) {
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout());
		container.setBorder(new EmptyBorder(30, 30, 30, 30));
		
		JPanel titreAnnonce = new JPanel();
		titreAnnonce.add(new JLabel(annonce.getType()));
		
		JPanel detailsAnnonce = new JPanel();
		detailsAnnonce.setLayout(new BorderLayout());
		
		JPanel debutDetails = new JPanel();
		debutDetails.setLayout(new BoxLayout(debutDetails, BoxLayout.PAGE_AXIS));
		
		
		JLabel numero = new JLabel("Numero: "+Integer.toString(annonce.getIdAnnonce()));
		JLabel position = new JLabel("Localisation: "+annonce.getPosition());
		JLabel prix = new JLabel("Prix: "+Float.toString(annonce.getPrix())+" �");
		JLabel descriptionLabel = new JLabel("Description: ");
		JTextArea description = new JTextArea(annonce.getDescription());
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dt = formatter.format(annonce.getDatePublication());
		JLabel date = new JLabel("Date: "+dt);
		
		
		debutDetails.add(numero);
		debutDetails.add(position);
		debutDetails.add(prix);
		description.setPreferredSize(new Dimension(400,600));
		description.setLineWrap(true);
		description.setEditable(false);
		debutDetails.add(date);
		debutDetails.add(descriptionLabel);
		
		JScrollPane descriptionDetails = new JScrollPane(description);
		detailsAnnonce.add(debutDetails, BorderLayout.NORTH);
		detailsAnnonce.add(descriptionDetails, BorderLayout.CENTER);
		
		JPanel boutonsContainer = new JPanel();
		if(VueAccueil.getUtilisateurConnecte() != null) {
			JButton faireOffre = new JButton("Faire offre");
			boutonsContainer.add(faireOffre);
			faireOffre.addActionListener(new FaireOffreControleur(this,VueAccueil.getUtilisateurConnecte(),annonce));
		}else {
			JLabel connexionRequise = new JLabel("Connectez vous pour faire une offre !");
			boutonsContainer.add(connexionRequise);
		}
		
		container.add(titreAnnonce, BorderLayout.NORTH);
		container.add(detailsAnnonce, BorderLayout.CENTER);
		container.add(boutonsContainer, BorderLayout.SOUTH);
		return container;

	}

}
