package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.NumberFormatter;

import controleur.AjoutAnnonceController;

public class VueCreationAnnonce extends JDialog{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<String> categorie;
	
	private JTextField typeChamp;
	private JTextField positionChamp;
	private JFormattedTextField prixChamp;
	private JComboBox<Object> categorieChamp;
	private JTextArea descriptionChamp;
	
	public VueCreationAnnonce(ArrayList<String> categorie) {
		super();	
		
		this.categorie = categorie;
		setContentPane(createContentAutenthification());
		setTitle("D�posez une annonce");
		setSize(new Dimension(400, 300));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
	}

	private JPanel createContentAutenthification() {
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout());
		
		JPanel champsContainer = new JPanel();
		champsContainer.setLayout(new BoxLayout(champsContainer, BoxLayout.Y_AXIS));
		
		JLabel typleLabel = new JLabel("Type");
		this.typeChamp = new JTextField(20);
		
		JLabel positionLabel = new JLabel("Localisation");
		this.positionChamp = new JTextField(20);
		
		NumberFormat format = NumberFormat.getIntegerInstance();
	    NumberFormatter formatter = new NumberFormatter(format);
	    formatter.setValueClass(Integer.class);
	    formatter.setMinimum(0);
	    formatter.setMaximum(Integer.MAX_VALUE);
	    formatter.setAllowsInvalid(false);
	    formatter.setCommitsOnValidEdit(true);
		JLabel prixLabel = new JLabel("Prix");
		this.prixChamp = new JFormattedTextField();
		
		JLabel categorieLabel = new JLabel("Cat�gorie");
		this.categorieChamp = new JComboBox<Object>(categorie.toArray());
		
		JLabel descriptionLabel = new JLabel("Description");
		this.descriptionChamp = new JTextArea(10, 1);
		
		JScrollPane containerTextArea = new JScrollPane(descriptionChamp);
		
		JButton validerAnnonce = new JButton("Deposez votre annonce");
		validerAnnonce.addActionListener(new AjoutAnnonceController(this));
		
		champsContainer.add(typleLabel);
		champsContainer.add(typeChamp);
		champsContainer.add(positionLabel);
		champsContainer.add(positionChamp);
		champsContainer.add(prixLabel);
		champsContainer.add(prixChamp);
		champsContainer.add(categorieLabel);
		champsContainer.add(categorieChamp);
		champsContainer.add(descriptionLabel);
		champsContainer.add(containerTextArea);
		
		
		container.add(champsContainer, BorderLayout.CENTER);
		container.add(validerAnnonce, BorderLayout.SOUTH);
		return container;
	}

	public JTextField getTypeChamp() {
		return typeChamp;
	}

	public JTextField getPositionChamp() {
		return positionChamp;
	}

	public JFormattedTextField getPrixChamp() {
		return prixChamp;
	}

	public JComboBox<Object> getCategorieChamp() {
		return categorieChamp;
	}

	public JTextArea getDescriptionChamp() {
		return descriptionChamp;
	}
	
}
