package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controleur.ConnexionControleur;
import controleur.InscriptionControleur;

public class VueAccueilAuthentifie extends JDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField champsNom;
	private JTextField champsMdp;
	private JLabel information;
	private JTextField champsMdpVerif;
	private VueAccueil vueAccueil;

	public VueAccueilAuthentifie(VueAccueil vue,boolean inscription) {
		super();	
		// si inscription = false on lance le popup d'authentifiaction sinon d'inscription
		this.vueAccueil = vue;
		if(inscription) {
			setContentPane(createContentInscription());
			setTitle("Inscrivez vous");	
		}
		else {
			setContentPane(createContentAutenthification());
			setTitle("Connectez vous");	
		}
		
		setSize(new Dimension(400, 300));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
	}
	
	private JPanel createContentAutenthification() {
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout());
		
		JPanel champsContainer = new JPanel();
		JPanel boutonContainer = new JPanel();
		JPanel nomContainer = new JPanel();
		JPanel mdpContainer = new JPanel();
		
		JLabel labelNom = new JLabel("Nom Utilisateur");
		this.champsNom = new JTextField(15);
		
		JLabel labelMdp = new JLabel("Mot de passe");
		this.champsMdp = new JPasswordField(15);
		
		this.information = new JLabel("Remplissez les champs pour vous connecter");
		
		JButton validerAuth = new JButton("Se Connecter");
		validerAuth.addActionListener(new ConnexionControleur(this, vueAccueil));
		
		nomContainer.add(labelNom);
		nomContainer.add(champsNom);
		mdpContainer.add(labelMdp);
		mdpContainer.add(champsMdp);
		
		champsContainer.setLayout(new BoxLayout(champsContainer, BoxLayout.Y_AXIS));
		champsContainer.add(nomContainer);
		champsContainer.add(mdpContainer);
		boutonContainer.add(validerAuth);
		
		container.add(champsContainer, BorderLayout.NORTH);
		container.add(information, BorderLayout.CENTER);
		container.add(boutonContainer, BorderLayout.SOUTH);		
		
		return container;
	}
	
	private JPanel createContentInscription() {
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout());
		
		JPanel champsContainer = new JPanel();
		JPanel boutonContainer = new JPanel();
		JPanel nomContainer = new JPanel();
		JPanel mdpContainer = new JPanel();
		JPanel mdpVerifContainer = new JPanel();
		
		JLabel labelNom = new JLabel("Nom Utilisateur");
		this.champsNom = new JTextField(15);
		
		JLabel labelMdp = new JLabel("Mot de passe");
		this.champsMdp = new JPasswordField(15);
		JLabel labelMdpVerif = new JLabel("Verifiez le Mot de passe");
		this.champsMdpVerif = new JPasswordField(15);
		
		this.information = new JLabel("Remplissez les champs pour vous inscrire");
		
		JButton validerAuth = new JButton("S'enregister");
		validerAuth.addActionListener(new InscriptionControleur(this));
		
		nomContainer.add(labelNom);
		nomContainer.add(champsNom);
		mdpContainer.add(labelMdp);
		mdpContainer.add(champsMdp);
		mdpVerifContainer.add(labelMdpVerif);
		mdpVerifContainer.add(champsMdpVerif);
		
		champsContainer.setLayout(new BoxLayout(champsContainer, BoxLayout.Y_AXIS));
		champsContainer.add(nomContainer);
		champsContainer.add(mdpContainer);
		champsContainer.add(mdpVerifContainer);
		boutonContainer.add(validerAuth);
		
		container.add(champsContainer, BorderLayout.NORTH);
		container.add(information, BorderLayout.CENTER);
		container.add(boutonContainer, BorderLayout.SOUTH);
		
		//attachement des controleurs
		
		
		return container;
	}

	public JTextField getChampsNom() {
		return champsNom;
	}

	public void setChampsNom(String champsNom) {
		this.champsNom.setText(champsNom);
	}

	public JTextField getChampsMdp() {
		return champsMdp;
	}

	public void setChampsMdp(String champsMdp) {
		this.champsMdp.setText(champsMdp);
	}

	public JLabel getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information.setText(information);
	}

	public JTextField getChampsMdpVerif() {
		return champsMdpVerif;
	}

	public void setChampsMdpVerif(String champsMdpVerif) {
		this.champsMdpVerif.setText(champsMdpVerif);
	}
	
	

}
