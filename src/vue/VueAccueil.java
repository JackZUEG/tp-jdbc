package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;

import controleur.AnnonceDetailsControleur;
import controleur.ConnexionInscriptionAcceuilControleur;
import controleur.CreationAnnonceControleur;
import controleur.DeconnexionControleur;
import controleur.DetailsOffresControleur;
import controleur.RechercheAnnonceControleur;
import dao.DaoAnnonce;
import modele.Annonce;
import modele.Categorie;
import modele.Offre;
import modele.Utilisateur;
import modeleTables.AnnoncesTableModele;


/**
 * Vue principale de l'application principale
 */
public class VueAccueil extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private AnnoncesTableModele annoncesModele;
	private AnnoncesTableModele mesAnnoncesModele;
	private AnnoncesTableModele mesOffresModele;
	private static Utilisateur utilisateurConnecte;
	private JPanel infos;
	private JLabel identifiantUtilisateur;
	private JButton connexion;
	private JButton inscription;
	private JButton ajoutAnnonce;
	private JTabbedPane ongletsPanel;
	
	/**
	 * Initialise la vue
	 * @param ArrayList<Categorie> liste des Categories d'annonces
	 * @param Utilisateur utilisateur qui se connecte � l'application
	 */
	public VueAccueil(ArrayList<Categorie> listeCategorie) {
		this.infos = new JPanel();
		this.identifiantUtilisateur = new JLabel();
		this.ongletsPanel = new JTabbedPane();
		
		setContentPane(createContent(listeCategorie));
		setTitle("Recherche une annonce");
		setPreferredSize(new Dimension(800, 600));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
	}
	
	/**
	 * Cr�e le panneau principal de la vue
	 * @param ArrayList<Categorie> liste des Categories d'annonces
	 * @return JPanel panneau principal de la vue
	 */
	private JPanel createContent(ArrayList<Categorie> listeCategorie) {
		
		
		//Panels de l'application
		JPanel container = new JPanel();
		JPanel hautContainer = new JPanel();
		JPanel rechercheAnnoncePanel = new JPanel();
		JPanel mesAnnoncesPanel = new JPanel();
		JPanel lesOffresPanel = new JPanel();
		
		container.setLayout(new BorderLayout());
		container.setBorder(new EmptyBorder(30, 30, 30, 30));
		hautContainer.setLayout(new BorderLayout());
		rechercheAnnoncePanel.setLayout(new BorderLayout());
		mesAnnoncesPanel.setLayout(new BorderLayout());
		lesOffresPanel.setLayout(new BorderLayout());
		
		//========= Onglet recherche ============
		
		//On cache par d�faut le panneau d'infos de connexion
		infos.setVisible(false);
		infos.add(identifiantUtilisateur);
		JButton deconnexion = new JButton("Deconnexion");
		deconnexion.addActionListener(new DeconnexionControleur(this));
		infos.add(deconnexion);
		
		JPanel filtresContainer = new JPanel();
		
		NumberFormat format = NumberFormat.getIntegerInstance();
	    NumberFormatter formatter = new NumberFormatter(format);
	    formatter.setValueClass(Integer.class);
	    formatter.setMinimum(0);
	    formatter.setMaximum(Integer.MAX_VALUE);
	    formatter.setAllowsInvalid(false);
	    formatter.setCommitsOnValidEdit(true);
	   
		JLabel prixMinLbl = new JLabel("  Prix min");
		JFormattedTextField prixMinField = new JFormattedTextField(formatter);
		JLabel prixMaxLbl = new JLabel("  Prix max");
		JFormattedTextField prixMaxField = new JFormattedTextField(formatter);
		JTextField localisationField = new JTextField();
		JButton recherche = new JButton("Rechercher");
		
		ArrayList<String> nomCategorie = new ArrayList<String>();
		for(int i = 0; i < listeCategorie.size(); i++) {
			nomCategorie.add(listeCategorie.get(i).getNom());
		}
		JComboBox categories = new JComboBox(nomCategorie.toArray());
		categories.setPreferredSize(new Dimension(100,30));
		
		prixMinField.setPreferredSize(new Dimension(50,30));
		prixMaxField.setPreferredSize(new Dimension(50,30));
		localisationField.setPreferredSize(new Dimension(50,30));
		
		filtresContainer.add(new JLabel("Categorie"));
		filtresContainer.add(categories);
		filtresContainer.add(prixMinLbl);
		filtresContainer.add(prixMinField);
		filtresContainer.add(new JLabel("�"));
		filtresContainer.add(prixMaxLbl);
		filtresContainer.add(prixMaxField);
		filtresContainer.add(new JLabel("�"));
		filtresContainer.add(new JLabel("  Localisation"));
		filtresContainer.add(localisationField);
		filtresContainer.add(recherche);
		
		recherche.addActionListener(new RechercheAnnonceControleur(this, categories, prixMinField, prixMaxField, localisationField));
		
		JPanel annoncesContainer = new JPanel();

		annoncesModele = new AnnoncesTableModele(new ArrayList<Annonce>());
		JTable annoncesTable = new JTable();
		annoncesTable.getTableHeader().setReorderingAllowed(false);
		annoncesTable.setModel(annoncesModele);
		
		annoncesTable.addMouseListener(new AnnonceDetailsControleur(this));
		annoncesContainer.add(new JScrollPane(annoncesTable), BorderLayout.CENTER);
		
		JPanel boutonsContainer = new JPanel();
		//Boutons pour s'inscrire ou se connecter
		this.connexion = new JButton("Se connecter");
		boutonsContainer.add(connexion);
		connexion.addActionListener(new ConnexionInscriptionAcceuilControleur(this,false));
		
		this.inscription = new JButton("S'enregistrer");
		boutonsContainer.add(inscription);
		inscription.addActionListener(new ConnexionInscriptionAcceuilControleur(this,true));
		
		this.ajoutAnnonce = new JButton("Ajouter une annonce");
		this.ajoutAnnonce.setVisible(false);
		this.ajoutAnnonce.addActionListener(new CreationAnnonceControleur(nomCategorie));
		boutonsContainer.add(ajoutAnnonce);
		
		hautContainer.add(infos, BorderLayout.NORTH);
		container.add(hautContainer, BorderLayout.NORTH);
		container.add(ongletsPanel, BorderLayout.CENTER);
		
		ongletsPanel.add("Recherche",rechercheAnnoncePanel);
		ongletsPanel.add("Mes annonces",mesAnnoncesPanel);
		ongletsPanel.add("Offres",lesOffresPanel);
		
		ongletsPanel.setEnabledAt(1,false);
		ongletsPanel.setEnabledAt(2,false);
		
		rechercheAnnoncePanel.add(filtresContainer, BorderLayout.NORTH);
		rechercheAnnoncePanel.add(filtresContainer, BorderLayout.NORTH);
		rechercheAnnoncePanel.add(annoncesContainer, BorderLayout.CENTER);
		rechercheAnnoncePanel.add(boutonsContainer, BorderLayout.SOUTH);
		
		//=========== Onglet Mes Annonces ================
		mesAnnoncesModele = new AnnoncesTableModele(new ArrayList<Annonce>());
		JTable listeAnnonce = new JTable();
		listeAnnonce.getTableHeader().setReorderingAllowed(false);
		listeAnnonce.setModel(mesAnnoncesModele);
		listeAnnonce.addMouseListener(new DetailsOffresControleur());

		mesAnnoncesPanel.add(new JScrollPane(listeAnnonce), BorderLayout.CENTER);
		
		
		//=========== Onglet Mes Offres ===============
		mesOffresModele = new AnnoncesTableModele(new ArrayList<Annonce>());
		JTable listeOffres = new JTable();
		listeOffres.getTableHeader().setReorderingAllowed(false);
		listeOffres.setModel(mesOffresModele);
		
		lesOffresPanel.add(new JScrollPane(listeOffres), BorderLayout.CENTER);
		
		
		
		return container;
	}
	
	/**
	 * Permet de mettre � jour la table des annonces
	 * @param ArrayList<Annonce> liste des annonces � mettre � jour dans la table
	 */
	public void majAnnonces(ArrayList<Annonce> listeAnnonces) {
		annoncesModele.update(listeAnnonces);
		annoncesModele.fireTableDataChanged();
	}
	
	/**
	 * Permet de mettre � jour la table des annonces post�e par l'utilisateur
	 * @param ArrayList<Annonce> liste des annonces � mettre � jour dans la table
	 */
	public void majMesAnnonces(ArrayList<Annonce> listeAnnonces) {
		mesAnnoncesModele.update(listeAnnonces);
		mesAnnoncesModele.fireTableDataChanged();
	}
	
	/**
	 * Permet de mettre � jour la table des offres de l'utilisateur
	 * @param ArrayList<Annonce> liste des annonces � mettre � jour dans la table
	 */
	public void majMesOffres(ArrayList<Annonce> listeAnnonces) {
		mesOffresModele.update(listeAnnonces);
		mesOffresModele.fireTableDataChanged();
	}
	
	/**
	 * Permet d'afficher les informations de connexion et les fonctionnalit�s en �tant connect�
	 * @param Utilisateur l'utilisateur connect� � l'application
	 */
	public void setVisibleConnexion(Utilisateur utilisateur) {
		infos.setVisible(true);
		identifiantUtilisateur.setText("Bienvenue, "+utilisateur.getIdentifiant());
		connexion.setVisible(false);
		inscription.setVisible(false);
		ajoutAnnonce.setVisible(true);
		setUtilisateurConnecte(utilisateur);
		ongletsPanel.setEnabledAt(1,true);
		ongletsPanel.setEnabledAt(2,true);
		
		majMesAnnonces(DaoAnnonce.listeMesAnnonces(utilisateur));
		majMesOffres(DaoAnnonce.listeAnnoncesMesOffres(utilisateur));
	}
	
	/**
	 * Permet de cacher les informations de connexion et les fonctionnalit�s en �tant connect�
	 */
	public void setInvisibleConnexion() {
		infos.setVisible(false);
		identifiantUtilisateur.setText("");
		connexion.setVisible(true);
		inscription.setVisible(true);
		ajoutAnnonce.setVisible(false);
		setUtilisateurConnecte(null);
		ongletsPanel.setEnabledAt(1,false);
		ongletsPanel.setEnabledAt(2,false);
		
	}
	
	public void setUtilisateurConnecte(Utilisateur utilisateur) {
		VueAccueil.utilisateurConnecte = utilisateur;
	}
	
	public static Utilisateur getUtilisateurConnecte() {
		return utilisateurConnecte;
	}
	
	
}
