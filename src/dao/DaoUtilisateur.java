package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import modele.Utilisateur;

public class DaoUtilisateur {
	public static Utilisateur getUtilisateur(String nom, String mdp) throws Exception { 
		Utilisateur user = null;
		String requete  = "SELECT * FROM UTILISATEUR WHERE identifiant = ? AND mdp = ?";
		try {
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setString(1, nom);
			stmt.setString(2, mdp);
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next())
				user = new Utilisateur(rs.getInt("idUtilisateur"), rs.getString("identifiant"), rs.getString("mdp"));
			else
				throw new Exception("Informations incorrectes");
			
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}
	
	
	public static Utilisateur getUtilisateur(int idUtilisateur) throws Exception { 
		Utilisateur user = null;
		String requete  = "SELECT * FROM UTILISATEUR WHERE idUtilisateur = ?";
		try {
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setInt(1, idUtilisateur);
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next())
				user = new Utilisateur(rs.getInt("idUtilisateur"), rs.getString("identifiant"), rs.getString("mdp"));
			else
				throw new Exception("Informations incorrectes");
			
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}
	
	
	
	public static Utilisateur inscireUtilisateur(String nom, String mdp) throws Exception {
		Utilisateur user = null;
		int id = 0;
		
		String requeteVerif  = "SELECT * FROM UTILISATEUR WHERE identifiant = ?";
		String requeteDernierID = "SELECT idUtilisateur FROM UTILISATEUR ORDER BY idUtilisateur DESC LIMIT 1";
		String requeteAjout = "INSERT INTO utilisateur(idUtilisateur, identifiant, mdp) VALUES(?,?,?)";
		try {
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requeteVerif);
			stmt.setString(1, nom);
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next())
				throw new Exception("Le nom est d�j� pris");
			else {
				
				stmt = Bd.getConnection().prepareStatement(requeteDernierID);
				rs = stmt.executeQuery();
				if(rs.next())
					id = rs.getInt(1)+1;
				
				stmt = Bd.getConnection().prepareStatement(requeteAjout);
				stmt.setInt(1, id);
				stmt.setString(2, nom);
				stmt.setString(3, mdp);
				stmt.executeUpdate();
			}
				
			user = new Utilisateur(id, nom, mdp);
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}
}
