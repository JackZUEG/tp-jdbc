package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

public class Bd {
	private static Connection connection;
	
	public static void init() {
		if(connection != null) {
			return;
		}
		ResourceBundle bundle = ResourceBundle.getBundle("config.config");
		try {
			Class.forName(bundle.getString("sgbd.driver"));
		} catch (ClassNotFoundException ex1) {
			System.out.println("Pilote non trouve!");
			System.exit(1);
		}
		
		try {
			Properties userInfo = new Properties();
			String mySqlUrl = bundle.getString("sgbd.url");
			userInfo.put("user", bundle.getString("sgbd.login"));
			userInfo.put("password", bundle.getString("sgbd.password"));
			connection = DriverManager.getConnection(mySqlUrl,userInfo);
		} catch (SQLException e) {
			System.out.println("Erreur de connexion � la BD");
			close();
			System.exit(1);
		}
	}
	
	public static void close() {
		if(connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Connection getConnection() {
		return connection;
	}
}
