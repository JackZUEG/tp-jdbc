package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Timer;

import dao.DaoUtilisateur;
import vue.VueAccueilAuthentifie;

public class InscriptionControleur implements ActionListener{

	private VueAccueilAuthentifie vue;

	public InscriptionControleur(VueAccueilAuthentifie vue) {
		super();
		this.vue = vue;
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		String nom = this.vue.getChampsNom().getText();
		String mdp = this.vue.getChampsMdp().getText();
		String mdpVerif = this.vue.getChampsMdpVerif().getText();
		if(!mdp.equals(mdpVerif)) {
			this.vue.setInformation("Les mots de passes ne correspondent pas");
		}
		else if(nom.equals("") || mdp.equals("") || mdpVerif.equals("")) {
			this.vue.setInformation("Veuillez entrer des infos valides");
		}
		else {
			try {
				DaoUtilisateur.inscireUtilisateur(nom, mdp);
				this.vue.setInformation("Vous �tes Inscrit");

				fermerVue();
				
			} catch (Exception e1) {
				this.vue.setInformation(e1.getMessage());
			}
		}

	}
	
	private void fermerVue() {
		Timer timer = new Timer (1000, new AbstractAction() {
			  /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			  public void actionPerformed(ActionEvent ae) {
			    vue.dispose();
			  }
			});
		timer.setRepeats(false);//the timer should only go off once
		timer.start();
	}

}
