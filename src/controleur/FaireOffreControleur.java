package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JOptionPane;

import dao.DaoOffre;
import modele.*;
import vue.VueDetailsAnnonce;

public class FaireOffreControleur implements ActionListener{
	private Utilisateur demandeur;
	private Annonce annonce;
	private VueDetailsAnnonce vue;
	
	public FaireOffreControleur(VueDetailsAnnonce vue, Utilisateur demandeur, Annonce annonce) {
		this.vue = vue;
		this.demandeur = demandeur;
		this.annonce = annonce;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

		Offre offre = new Offre(date, null, annonce, demandeur);
		if(DaoOffre.verifExisteOffre(offre)) {
			JOptionPane.showMessageDialog(vue, "Vous avez deja effectue une offre sur cette annonce!");
		}else {
			JOptionPane.showMessageDialog(vue, "Votre offre a bien �t� enregistr�e");
			DaoOffre.creationOffre(offre);
		}
	
		vue.dispose();
		
		
	}

}
