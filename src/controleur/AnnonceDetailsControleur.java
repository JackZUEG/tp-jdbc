package controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JTable;

import dao.DaoAnnonce;
import dao.DaoOffre;
import modele.Annonce;
import modele.Offre;
import vue.VueAccueil;
import vue.VueDetailsAnnonce;

public class AnnonceDetailsControleur implements MouseListener{
	
	private VueAccueil vue;
	
	public AnnonceDetailsControleur(VueAccueil vue) {
		super();
		this.vue = vue;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		JTable table = (JTable) e.getSource();
		
		if(e.getClickCount() == 2) { // On v�rifie si il s'agit bien d'un double-clic
	        
	        if (table.getSelectedRow() != -1) { // On v�rifie si une ligne est s�lectionn�e
	        	Annonce annonceSelec = DaoAnnonce.detailsAnnonce((Integer)table.getValueAt(table.getSelectedRow(),0));
	        	// On ouvre la fenetre
	        	VueDetailsAnnonce annonceDetails = new VueDetailsAnnonce(annonceSelec);
	        	annonceDetails.setVisible(true);
	        }
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
