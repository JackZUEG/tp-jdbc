package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import dao.DaoAnnonce;
import modele.Annonce;
import vue.*;

public class RechercheAnnonceControleur implements ActionListener{
	private VueAccueil vueAccueil;
	private JComboBox categorie;
	private JTextField prixMin;
	private JTextField prixMax;
	private JTextField localisation;

	
	public RechercheAnnonceControleur(VueAccueil vueAccueil, JComboBox categorie, JTextField prixMin,
			JTextField prixMax, JTextField localisation) {
		this.vueAccueil = vueAccueil;
		this.categorie = categorie;
		this.prixMin = prixMin;
		this.prixMax = prixMax;
		this.localisation = localisation;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			ArrayList<Annonce> listeAnnonce = DaoAnnonce.listeAnnonces(categorie.getSelectedItem().toString(), prixMin.getText().replace("\u202F",""), prixMax.getText().replace("\u202F",""), localisation.getText());
			vueAccueil.majAnnonces(listeAnnonce);
		} catch(Exception ex) {
			System.out.println(ex);
		}
		
	}
}
