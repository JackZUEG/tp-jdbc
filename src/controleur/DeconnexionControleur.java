package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import vue.VueAccueil;

public class DeconnexionControleur implements ActionListener{
	private VueAccueil vue;
	
	public DeconnexionControleur(VueAccueil vue) {
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		vue.setInvisibleConnexion();
		
	}
}
