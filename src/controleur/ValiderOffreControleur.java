package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import dao.DaoOffre;
import modele.Offre;
import vue.VueDetailsOffres;

public class ValiderOffreControleur implements ActionListener, MouseListener {

    VueDetailsOffres vue;

    public ValiderOffreControleur(VueDetailsOffres vue) {
        super();
        this.vue = vue;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Offre offre = vue.getOffreSelectionnee();
        DaoOffre.validerOffre(offre);
        vue.dispose();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        vue.enableButton();
    }




    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

}