package controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JTable;

import dao.DaoOffre;
import modele.Offre;
import vue.VueDetailsOffres;

public class DetailsOffresControleur implements MouseListener{

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		JTable table = (JTable) e.getSource();
		
		if(e.getClickCount() == 2) { // On v�rifie si il s'agit bien d'un double-clic
	       
	        if (table.getSelectedRow() != -1) { // On v�rifie si une ligne est s�lectionn�e
	        	ArrayList<Offre> listeOffre = DaoOffre.listeOffresAnnonce((Integer)table.getValueAt(table.getSelectedRow(),0));
	        	// On ouvre la fenetre
	        	VueDetailsOffres offresDetails = new VueDetailsOffres(listeOffre);
	        	offresDetails.setVisible(true);
	        }
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
