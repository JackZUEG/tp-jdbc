package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.Calendar;

import javax.swing.JOptionPane;

import dao.DaoAnnonce;
import modele.Annonce;
import modele.Categorie;
import vue.VueAccueil;
import vue.VueCreationAnnonce;

public class AjoutAnnonceController implements ActionListener{
	
	private VueCreationAnnonce vue;
	
	public AjoutAnnonceController(VueCreationAnnonce vue) {
		super();
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String type = vue.getTypeChamp().getText();
		String position = vue.getPositionChamp().getText();
		String prixString = vue.getPrixChamp().getText().replace("\u202F","");
		String description = vue.getDescriptionChamp().getText();
		Categorie categorie = new Categorie(-1, vue.getCategorieChamp().getSelectedItem().toString());
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		int idUtilisateur = VueAccueil.getUtilisateurConnecte().getIdUtilisateur();
		
		if(type.isEmpty() || position.isEmpty() || description.isEmpty() || prixString.isEmpty()) {
			JOptionPane.showMessageDialog(vue, "Renseignez tous les champs","Erreur",JOptionPane.ERROR_MESSAGE);
		}else {
			
			Float prix = Float.parseFloat(prixString);
			Annonce nouvAnnonce = new Annonce(-1, type, position, prix, description, date, idUtilisateur, categorie);
			DaoAnnonce.ajouterAnnonce(nouvAnnonce);
			
			vue.dispose();	
			
		}
		
	}

}
