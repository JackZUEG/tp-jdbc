package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import vue.VueAccueil;
import vue.VueAccueilAuthentifie;

public class ConnexionInscriptionAcceuilControleur implements ActionListener{
	
	private boolean inscription;
	private VueAccueil vue;
	
	public ConnexionInscriptionAcceuilControleur(VueAccueil vue, boolean inscription) {
		super();
		this.inscription = inscription;
		this.vue = vue;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		VueAccueilAuthentifie popupConnexion = new VueAccueilAuthentifie(vue,inscription);
		popupConnexion.setVisible(true);
	}

}
