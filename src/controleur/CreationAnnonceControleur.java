package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import vue.VueCreationAnnonce;

public class CreationAnnonceControleur implements ActionListener{
	private ArrayList<String> categorie;

	public CreationAnnonceControleur(ArrayList<String> categorie) {
		super();
		this.categorie = categorie;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		VueCreationAnnonce vue = new VueCreationAnnonce(this.categorie);
		vue.setVisible(true);
	}
	
}
