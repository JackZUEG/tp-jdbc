package modele;

/**
 * Classe représentant l'objet Offre
 */
public class Offre {
	private java.sql.Date dateOffre;
	private java.sql.Date dateAcceptation;
	private Annonce annonce;
	private Utilisateur demandeur;

	/**
	 * Constructeur de la classe offre
	 */
	public Offre(java.sql.Date dateOffre, java.sql.Date acceptation, Annonce annonce, Utilisateur demandeur) {
		super();
		this.dateOffre = dateOffre;
		this.dateAcceptation = acceptation;
		this.annonce = annonce;
		this.demandeur = demandeur;
	}
	
	//getters et setters
	public java.sql.Date getDateOffre() {
		return dateOffre;
	} 
	public void setDateOffre(java.sql.Date dateOffre) {
		this.dateOffre = dateOffre;
	}
	public java.sql.Date getAcceptation() {
		return dateAcceptation;
	}
	public void setAcceptation(java.sql.Date acceptation) {
		this.dateAcceptation = acceptation;
	}

	public Annonce getAnnonce() {
		return this.annonce;
	}

	public void setAnnonce(Annonce autreAnnonce) {
		this.annonce = autreAnnonce;
	}

	public Utilisateur getDemandeur() {
		return demandeur;
	}

	public void setDemandeur(Utilisateur demandeur) {
		this.demandeur = demandeur;
	}
	
	
}