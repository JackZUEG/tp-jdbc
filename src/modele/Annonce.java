package modele;

import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

/**
 * Classe représentant un objet Annonce
 */
public class Annonce {
	private int idAnnonce;
	private String type;
	private String position;
	private float prix;
	private String description;
	private Date datePublication;
	private int idUtilisateur;
	private Categorie categorie;
	private List<Offre> lesOffres;
	
	/**
	 * Constructeur de la classe Annonce
	 */
	public Annonce(int idAnnonce, String type, String position, float prix, String description, Date datePublication,
			int idUtilisateur, Categorie categorie) {
		this.idAnnonce = idAnnonce;
		this.type = type;
		this.position = position;
		this.prix = prix;
		this.description = description;
		this.datePublication = datePublication;
		this.idUtilisateur = idUtilisateur;
		this.categorie = categorie;
		this.lesOffres = new ArrayList<Offre>();
	}
	
	public void ajouterOffre(Offre offre) {
		lesOffres.add(offre);
	}
	
	public List<Offre> getOffres(){
		return lesOffres;
	}

	public int getIdAnnonce() {
		return idAnnonce;
	}
	
	public void setIdAnnonce(int id) {
		this.idAnnonce = id;
	}

	public String getType() {
		return type;
	}

	public String getPosition() {
		return position;
	}

	public float getPrix() {
		return prix;
	}

	public String getDescription() {
		return description;
	}

	public Date getDatePublication() {
		return datePublication;
	}

	public int getIdUtilisateur() {
		return idUtilisateur;
	}

	public Categorie getCategorie() {
		return categorie;
	}
	
}
