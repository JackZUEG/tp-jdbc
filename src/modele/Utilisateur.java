package modele;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe repr�sentant l'objet utilisateur
 */
public class Utilisateur {

	private int idUtilisateur;
	private String identifiant;
	private String mdp;
	private List<Annonce> sesAnnonces; //on recup�re les offres propos�es des annonces via les annonces
	private List<Offre> sesOffres; 

	/**
	 * Constructeur de l'objet utilisateur
	 */
	public Utilisateur(int idUtilisateur, String identifiant, String mdp) {
		super();
		this.idUtilisateur = idUtilisateur;
		this.identifiant = identifiant;
		this.mdp = mdp;
		this.sesAnnonces = new ArrayList<Annonce>();
		this.sesOffres = new ArrayList<Offre>();
	}
	
	
	//getter et setters
	public int getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public String getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}


	public List<Annonce> getSesAnnonces() {
		return sesAnnonces;
	}


	public void ajouterAnnonces(Annonce nouvelleAnnonce) {
		this.sesAnnonces.add(nouvelleAnnonce);
	}
	
	public void supprimerAnnonce() {
		
	}
	
	
}