package modeleTables;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import modele.*;

/**
 * Classe repr�sentant le mod�le de la table des Annonces
 */
public class AnnoncesTableModele extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private static final String[] COLUMNS = new String[] { "Numero","Type", "Localisation", "Prix", "Date"};
	private ArrayList<Annonce> listeAnnonces;
	
	/**
	 * Constructeur de la table
	 * @param ArrayList<Annonce> liste des annonces � mettre dans la table
	 */
	public AnnoncesTableModele(ArrayList<Annonce> listeAnnonces) {
		this.listeAnnonces = listeAnnonces;
	}

	@Override
	public int getColumnCount() {
		return COLUMNS.length;
	}
	
	@Override
    public String getColumnName(int col) {
        return COLUMNS[col];
    }

    @Override
    public Class<?> getColumnClass(int col) {
        return String.class;
    }

	@Override
	public int getRowCount() {
		return listeAnnonces.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		
		Annonce annonce = listeAnnonces.get(row);
		
		if(col == 0) {
			return annonce.getIdAnnonce();
		}
		if(col == 1) {
			return annonce.getType();
		}
		if(col == 2) {
			return annonce.getPosition();
		}
		if(col == 3) {
			return annonce.getPrix();
		}
		if(col == 4) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	        String date = formatter.format(annonce.getDatePublication());
			return date;
		}
		return "?";
	}
	
	@Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
	
	/**
	 * Permet de mettre � jour la table des annonces
	 * ArrayList<Annonce> liste des annonce � remplacer
	 */
	public void update(ArrayList<Annonce> listeAnnonces) {
		this.listeAnnonces = listeAnnonces;
	}

}
