package modeleTables;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import modele.Annonce;
import modele.Offre;

public class OffresTableModele extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String[] COLUMNS = new String[] {"Utilisateur", "Date Offre"};
	private ArrayList<Offre> listeOffres;
	
	public OffresTableModele(ArrayList<Offre> listeOffres) {
		this.listeOffres = listeOffres;
	}

	@Override
	public int getColumnCount() {
		return COLUMNS.length;
	}
	
	@Override
    public String getColumnName(int col) {
        return COLUMNS[col];
    }

    @Override
    public Class<?> getColumnClass(int col) {
        return String.class;
    }

	@Override
	public int getRowCount() {
		return listeOffres.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		Offre offre = listeOffres.get(row);
		
		if(col == 0) {
			return offre.getDemandeur().getIdentifiant();
		}
		if(col == 1) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	        String date = formatter.format(offre.getDateOffre());
			return date;
		}
		return "?";
	}
	
	@Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
	
	/**
	 * Permet de mettre � jour la table des annonces
	 * ArrayList<Annonce> liste des annonce � remplacer
	 */
	public void update(ArrayList<Offre> listeOffres) {
		this.listeOffres = listeOffres;
	}
	
	public Offre getOffre(int index) {
		return listeOffres.get(index);
		
	}

}
